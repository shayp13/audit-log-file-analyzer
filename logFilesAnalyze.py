import mysql.connector
from datetime import datetime
from os import listdir
from os.path import isfile, join
import os
import logging
import configparser


"""
The function reads all the file names of the files in this directory, if the configuration file exist then the
 function read the configurations into global variables containing the audit log files directory path and the database information. 
Input: None.
output: None.
"""
def reading_config():
    global dbInfo
    global logFilesInfo
    global LOG_FILES_PATH
    isConfigurationFile = False
    try:
        thisDirectoryFiles = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f))] #getting all the file names in this directory
        for file in thisDirectoryFiles:
            if file == "configfile.ini":    #looking for the configuration file
                isConfigurationFile = True
                config_obj = configparser.ConfigParser()
                config_obj.read(os.getcwd() + "\\" + file) #reading the configuration file
                logFilesInfo = config_obj["log_files_info"]
                dbInfo = config_obj["mySQL_info"]       #getting the configuration file sections
                LOG_FILES_PATH = logFilesInfo["path"]
                handle_logging("Getting config file successful", "INFO")
        if isConfigurationFile == False:
            handle_logging("Error in finding configuration file", "ERROR")

    except Exception:
        handle_logging("Error in finding configuration file", "ERROR")


"""
The function recives the database cursor, iterates through all the files in the log files directory and checking which files need to be checked,
the function does that by checking for every file if the last modification timestamp that is saved in the database is the same as the timestamp in the system.
if the timestamp is the same then that means that the file has not been changed since the last program run and there is no need in checking the file again.
Input: mycursor - the DB handler
output: list of files that need to be parser and inserted into the database.
"""
def handle_log_files_list(mycursor):
    filesToCheck = []
    handle_logging("getting log files", "DEBUG")
    try:
        onlyfiles = [f for f in listdir(LOG_FILES_PATH) if isfile(join(LOG_FILES_PATH, f))] #getting all the file names in the log file directory
    except Exception:
        handle_logging("could not get files", "ERROR")
    for file in onlyfiles:
        lastModified = get_last_time_modified(mycursor, file) #getting last modified timestamp from the database
        if lastModified == os.path.getmtime(LOG_FILES_PATH+file): #comparing the database timestamp to the system timestamp
            pass
        else:
            filesToCheck.append(file)

    return filesToCheck



"""
The function reads and return a file content according to his name.
Input: file - the file name
Ourput - the file content
"""
def get_log_file(file):
    try:
        logFileObj = open(LOG_FILES_PATH+file, "r")
        logFileContent = logFileObj.read()
        handle_logging("file data was read", "DEBUG")
    except Exception:
        handle_logging("Error in reading file", "ERROR")

    return logFileContent


"""
The function parses specific log-file contents into a dictionary of lists of records & their timestamps, keyed by command.
Input: logFileContent - the content of the log file, lastRecordTimestamp - the timestamp of the last record, this timestamp is used for not reading 
    a record that has been read before, if a record timestamp is lower then the last timestamp then this record is already in the database.
Output: dictOfLogCommands - a dictionary of lists of records & their timestamps, keyed by command
"""
def parse_log_files(logFileContent, lastRecordTimestamp):
    dictOfLogCommands = {}
    isValidTimestamp = False

    splittedLogFileByCalls = logFileContent.split("\n") #creating a list of separate records

    for logRecord in splittedLogFileByCalls: #iterating through every record in porpuse of finding the record command and timestamp
        recordArg = logRecord.split(" ")
        comm="" #record command
        timestamp="" #record timestamp
        isValidTimestamp = False

        for keyValuePair in recordArg: #iterating through every record variables and the value to find the timestamp and command
            splittedRecord = keyValuePair.split("=") #splitting to key and value
            if len(splittedRecord) == 2: #checking if the key has a value
                if splittedRecord[0] == "msg":
                    timestamp = splittedRecord[1]
                    if int(extract_timestamp(timestamp)) > lastRecordTimestamp:
                        isValidTimestamp = True
                    else:
                        pass

                elif splittedRecord[0] == "comm":
                    comm = splittedRecord[1]
                    pass

        if isValidTimestamp:
            if comm not in dictOfLogCommands:
                listOfCalls = []
            else:
                listOfCalls = dictOfLogCommands[comm]
            #creating a dict of log commands valued by record & timestamps
            listOfCalls.append([logRecord,timestamp])
            dictOfLogCommands[comm] = listOfCalls

    return dictOfLogCommands


"""
The function gets a msg variable in a record, extract and return the timestamp
Input: msg - the msg variable in a record
Output: timestamp - the record timestamp
"""
def extract_timestamp(msg):
    timestamp = msg.replace(".", "")
    timestamp = timestamp[timestamp.find("(")+1 : timestamp.find(":")]
    return timestamp

"""
The function connects to a mySQL database, connects to a schema, and if needed created the schema tables.
input: none
Output: mycursor - the handler to the schema, mydb - the handler to the database.
"""
def hanlde_DB():
    try:    #trying to connect to the database
        mydb = mysql.connector.connect(
  host=dbInfo["host"],
  user=dbInfo["user"],
  password=dbInfo["password"]
)
        handle_logging("mySQL is connected", "INFO")
    except Exception:
        handle_logging("ERRO connecting to database", "ERROR")

    mycursor = mydb.cursor()    #checking if the schema already exist
    sql = ("select * from information_schema.schemata where schema_name=%s")
    val = [dbInfo["schema_name"]]
    mycursor.execute(sql, val)
    result = mycursor.fetchall()

    if len(result) == 0: #creating schema if needed
        handle_logging("Creating database schema", "DEBUG")
        try:
            sql = ("CREATE DATABASE %s")
            val = [dbInfo["schema_name"]]
            mycursor.execute(sql, val)
            handle_logging("Database schema has been created", "INFO")
        except Exception:
            handle_logging("Error in creating database schema", "ERROR")

    try: #trying to connect to the database & schema
        mydb = mysql.connector.connect(
  host=dbInfo["host"],
  user=dbInfo["user"],
  password=dbInfo["password"],
  database= dbInfo["schema_name"]
)
        handle_logging("Database has been connected", "INFO")
    except Exception:
        handle_logging("Error in connecting to database", "ERROR")

    mycursor = mydb.cursor()

    #checking if the log_recors table already exist
    sql = ("select * from information_schema.tables where table_schema=%s and table_name ='log_records'")
    val = [dbInfo["schema_name"]]
    mycursor.execute(sql, val)
    result = mycursor.fetchall()

    if len(result) == 0: #creating log_record table if needed
        try:
            mycursor.execute(("CREATE TABLE log_records (id INT AUTO_INCREMENT PRIMARY KEY, command VARCHAR(50), record VARCHAR(12000), datetime datetime, timestamp VARCHAR(50))"))
            handle_logging(" log_record table was created", "INFO")

        except Exception:
            handle_logging("Error in creating log_records table", "ERROR")

    #checking if the files table already exist
    sql = ("select * from information_schema.tables where table_schema=%s and table_name ='files'")
    val = [dbInfo["schema_name"]]
    mycursor.execute(sql, val)
    result = mycursor.fetchall()
    if len(result) == 0:    #creating files table if needed
        try:
            mycursor.execute(("CREATE TABLE files (name VARCHAR(50), time_modified VARCHAR(50))"))
            handle_logging("Table was created", "INFO")
        except Exception:
            handle_logging("Error in creating table", "ERROR")


    return mycursor, mydb


"""
The function returns a timestamp of the last time that a certain file has been modified from the database
Input: mycursor - the schema handler, file - the file name
Output: the file modification timestamp or 0 if the file is not in the database.
"""
def get_last_time_modified(mycursor, file):
    sql = ("SELECT time_modified FROM files where name=%s")
    val = [file]
    mycursor.execute(sql, val)
    result = mycursor.fetchall()

    if len(result) == 0:
        return 0

    return result[0][0]

"""
The function returns the timestamp of last record that was inserted into the database.
Input: mycursor - the database schema handler
Output: the timestamp of the last record that was inserted into the database or 0 if there are no record in the database.
"""
def get_last_record_timestamp(mycursor):
    mycursor.execute("SELECT* FROM log_records")
    result = mycursor.fetchall()

    if len(result) > 0:
        mycursor.execute("SELECT MAX(timestamp) FROM log_records")
        result = mycursor.fetchall()
        return int(result[0][0])
    return 0


"""
The function insert all the record and record information(command, record, datetime, timestamp) into the database.
Input: mycursor - the database schema handler, recordByCommand - a dictionary that is keyed by a command and valued by a list of record and their timestamp, 
    mydb - the database handler.
Output: None.
"""
def insert_logs_to_database(mycursor, recordByCommand, mydb):
    for command in recordByCommand.keys(): #iterating through all of the record by the commands.
        for recordAndTimestamp in recordByCommand[command]: #iterating through every command&timestamp
            record = recordAndTimestamp[0]
            timestamp = extract_timestamp(recordAndTimestamp[1])  #extracting the timestamp
            datetimeTimestamp = int(recordAndTimestamp[1][recordAndTimestamp[1].find("(")+1: recordAndTimestamp[1].find(".")]) #removing the mili second time of the timestamp
            rowDatetime = datetime.fromtimestamp(datetimeTimestamp) #creating a datetime object using the  datetime.fromtimestamp function
            sql = ("INSERT INTO log_records (command, record, datetime, timestamp) VALUES(%s, %s, %s, %s)")
            val = command, record, rowDatetime, timestamp   #inserting into the database
            try:
                mycursor.execute(sql, val)
                handle_logging("Inserting into database", "DEBUG")
            except Exception:
                handle_logging("Error in inserting to database", "ERROR")

    mydb.commit()


"""
The function inserts a log file information into the database.
Input: mycursor - the database schema handler, mydb - the database handler, filename - the name of the log file, timeModified - the last time that the log file was modified.
Output: None.
"""
def insert_file_into_database(mycursor, mydb, fileName, timeModified):
    sql = ("SELECT * FROM files where name=%s")
    val = [fileName]
    mycursor.execute(sql, val)  #checking if a log file is already in the database, if it is then there is only need in updating the modification timestamp, otherwise adding the file to the database.
    result = mycursor.fetchall()
    if len(result) == 0:
        sql = ("INSERT INTO files (name, time_modified) VALUES(%s, %s)")
        val = fileName, timeModified
    else:
        sql = ("UPDATE files set time_modified=%s where name=%s")
        val = timeModified, fileName
    try:
        mycursor.execute(sql, val)
        handle_logging("inserting or updating files table in database", "DEBUG")
    except Exception:
        handle_logging("Error in inserting or updating files table", "ERROR")
    mydb.commit()


"""
The function handles the logging of the program to a txt file.
Input: logMessage - the logging message that needs to be written in the log file of the program, level - the level of the message (DEBUG, INFO, ERROR)

"""
def handle_logging(logMessage, level):
    logging.basicConfig(filename="LoggingOfAuditLogFile.txt", level=logging.DEBUG) #creating or opening the logging file

    if level == "DEBUG":
        logging.debug(logMessage) #writing into the logging file

    elif level == "INFO":
        logging.info(logMessage) #writing into the logging file

    elif level == "ERROR":
        logging.error(logMessage) #writing into the logging file



def main():
    reading_config() #reading the program fonfiguration file
    mycursor, myDB = hanlde_DB() #connecting to the database
    filesToCheck = handle_log_files_list(mycursor) #getting the needed files to checks and insert to the database.
    handle_logging("getting file list was successful", "INFO")
    lastRecordTimestamp = get_last_record_timestamp(mycursor)
    for file in filesToCheck: #iterating through every log file
        try:
            logFileContent = get_log_file(file) #getting the content of the log file
            handle_logging("Getting file contant was successful", "INFO")
        except Exception:
            handle_logging("Error in getting file content", "ERROR")
        try:
            recordByCommand = parse_log_files(logFileContent, lastRecordTimestamp) #parsing the log file content
            handle_logging("Parsing file was successful", "INFO")
        except Exception:
            handle_logging("Error in parsing file", "ERROR")
        insert_logs_to_database(mycursor, recordByCommand, myDB) #inserting the record info to the database
        handle_logging("Records were inserted into database", "INFO")
        insert_file_into_database(mycursor, myDB, file, os.path.getmtime(LOG_FILES_PATH+file)) #inserting the log file info to the database
        handle_logging("file info was inserted into database", "INFO")



if __name__ == "__main__":
    main()
